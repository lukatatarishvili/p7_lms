<?php 
session_start();
include "header.php"; 
include 'connection.php';
?>

        <!-- page content area main -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Issue books</h3>
                    </div>

                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="row" style="min-height:500px">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Issue books</h2>

                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                               <form name="form1" action="" method="POST">
                                <TABLE>
                                   <tr>
                                       <td>
                                           <select name="enr" class="form-control selectpicker">
                                               <?php 
                                               $res=mysqli_query($link, "SELECT enrollment FROM student_registration");
                                               while ($row=mysqli_fetch_array($res)) {
                                                   echo "<option>";
                                                   echo $row['enrollment'];
                                                   echo "</option>";
                                               }
                                                ?>
                                           </select>
                                       </td>
                                       <td>
                                           <input type="submit" value="Search" name="submit1" 
                                           class="form-control btn btn-default" style="margin-top: 5px;">
                                       </td>
                                   </tr>
                                   </TABLE>
                               
                               <?php 
                               if (isset($_POST["submit1"])) {  
                                $res= mysqli_query($link, "SELECT * FROM student_registration where enrollment='$_POST[enr]' ");
                                while ($row=mysqli_fetch_array($res)) {
                                    $firstname=$row["firstname"];
                                    $lastname=$row["lastname"];
                                    $username=$row["username"];
                                    $email=$row["email"];
                                    $contact=$row["contact"];
                                    $sem=$row["sem"];
                                    $enrollment=$row["enrollment"];
                                    $_SESSION["enrollment"]=$enrollment;
                                    $_SESSION["susername"]=$username;
                                }

                                ?>


                                <table class="table table-bordered">
                                        <tr>
                                             <td> <input type="text"   value="<?php echo $enrollment; ?>" class="form-control" placeholder="enrollment" name="enrollment" disabled=""></td>
                                        </tr>

                                        <tr>
                                             <td> <input type="text" class="form-control" placeholder="studentname" name="studentname" value="<?php echo $firstname.' '.$lastname; ?>" required=""></td>
                                        </tr>

                                        <tr>
                                             <td> <input type="text" class="form-control" placeholder="studentsem" name="studentsem" value="<?php echo $sem; ?>" required=""></td>
                                        </tr>

                                        <tr>
                                             <td> <input type="text" class="form-control" placeholder="studentcontact" name="studentcontact" value="<?php echo $contact; ?>" required=""></td>
                                        </tr>

                                        <tr>
                                             <td> <input type="text" class="form-control" placeholder="studentemail" name="studentemail" value="<?php echo $email; ?>" required=""></td>
                                        </tr>
                                        <tr>
                                             <td><select name="booksname" class="form-control selectpicker">
                                                 <?php 

                                                 $res=mysqli_query($link, "SELECT books_name FROM add_books");
                                                 while ($row=mysqli_fetch_array($res)) {
                                                    echo  "<option>";
                                                    echo $row["books_name"];
                                                     echo "</option>";
                                                 }
                                                  ?>
                                                
                                            </select></td>
                                        </tr>
                                    
                                          <tr>
                                             <td> <input type="text" class="form-control" placeholder="booksissuedate"  value="<?php echo date("d-m-y");?>" name="booksissuedate" required=""></td>
                                        </tr>

                                        


                                          <tr>
                                             <td> <input type="text" class="form-control" placeholder="studentusername"  value="<?php echo $username;?>" name="studentusername" disabled=""></td>
                                        </tr>


                                          <tr>
                                             <td> <input type="submit"   name="submit2" class="form-control btn btn-primary" name="issue books" value="Issue books"></td>
                                        </tr>
                                    </table>
                                <?php
                               }

                                ?>
                            </form>


<?php 
    // if (isset($_POST['submit2'])) {
    //    mysqli_query($link, "INSERT INTO issue_books values('','$_SESSION[enrollment]','$_POST[studentname]','$_POST[studentsem]','$_POST[studentcontact]','$_POST[studentemail]','$_POST[booksname]','$_POST[booksissuedate]','$_SESSION[susername]')"); 


 if (isset($_POST["submit2"])){

                            

      $qty=0;
      $res=mysqli_query($link, "SELECT * FROM add_books where books_name='$_POST[booksname]' ");
      while ($row=mysqli_fetch_array($res)) {
        $qty=$row["available_qty"];
      }
      if ($qty==0) { ?>
         <div class="alert alert-danger col-lg-6 col-lg-push-3">
    <strong style="color:white">This books are not available in stock</strong> 
</div>
    <?php  } else{



       mysqli_query($link,"INSERT INTO issue_books values('','$_SESSION[enrollment]','$_POST[studentname]','$_POST[studentsem]','$_POST[studentcontact]','$_POST[studentemail]','$_POST[booksname]','$_POST[booksissuedate]', '', '$_SESSION[susername]') ");
       mysqli_query($link, "UPDATE add_books set available_qty=available_qty-1 where books_name = '$_POST[booksname]' ");

   ?> 
      <script type="text/javascript">
           alert("Books issued succesfully");
           windows.location.href=windows.location.href;
       </script> 
 <?php } 
       }

         
?>






                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->


        <?php include 'footer.php'; ?>
